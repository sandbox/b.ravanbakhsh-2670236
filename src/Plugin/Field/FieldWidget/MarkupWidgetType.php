<?php

/**
 * @file
 * Contains \Drupal\markup_field\Plugin\Field\FieldWidget\MarkupWidgetType.
 */

namespace Drupal\markup_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'markup_widget_type' widget.
 *
 * @FieldWidget(
 *   id = "markup_widget_type",
 *   label = @Translation("Markup widget type"),
 *   field_types = {
 *     "markup_field_type"
 *   }
 * )
 */
class MarkupWidgetType extends WidgetBase {
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'element' => 'p',
      'classes' => '',
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['element'] = array(
      '#title' => $this->t('Element'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('element'),
      '#description' => $this->t('E.g. div, p, section etc.'),
      '#weight' => 1,
    );
    $elements['classes'] = array(
      '#title' => t('Wrapper CSS classes'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('classes'),
      '#weight' => 11,
      '#element_validate' => array('field_group_validate_css_class'),
    );

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    if ($this->getSetting('element')) {
      $summary[] = $this->t('Element: @element', array('@element' => $this->getSetting('element')));
    }
    if ($this->getSetting('classes')) {
      $summary[] = \Drupal::translation()->translate('Extra CSS classes: @classes', array('@classes' => $this->getSetting('classes')));
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];

    $widget_settings = $this->getSettings();
    $description = $this->fieldDefinition->getDescription();
    $element['#markup'] = $description;
    $element['#prefix'] = '<' . $widget_settings['element'] . ' class="' . $widget_settings['classes'] . '">';
    $element['#suffix'] = '</' . $widget_settings['element'] . '>';

    return $element;
  }

}
